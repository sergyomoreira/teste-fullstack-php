<?php


namespace App\Controllers;


use App\Core\Controller;
use App\Core\Session;
use App\Models\Auth;
use App\Models\User;


class Users extends Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!Auth::user()){
            redirect('login');
        }
    }

    public function index(): void
    {
        $users = (new User())->find()->fetch(true);


        echo $this->load('users/index',[
            "url_base" => url(),
            "user"=> Auth::user(),
            "users" => $users,
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }

    public function create()
    {
        echo $this->load('users/create',[
            "url_base" => url(),
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }

    public function store(array $request)
    {
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);
        $addUser = (new User)->bootstrap(
            $request['first_name'],
            $request['last_name'],
            $request['email'],
            $request['password']
        );

        if (!$addUser->save()){
            $this->message->error($addUser->message()->getText());
        }
        $this->message->success('Usuário cadastrado com sucesso!!')->flash();
        redirect('users');
    }

    public function edit($id)
    {
        $user = (new User())->findById(intval($id));

        echo $this->load('users/edit',[
            "url_base" => url(),
            "user"=> Auth::user(),
            "user" => $user,
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }

    public function update(array $request)
    {
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $user = (new User())->findById(intVal($request['id']));

        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];

        if (!empty($request['password'])){
            $user->password = $request['password'];
        }

        if (($user->save())){
            $this->message->success('Usuário atualizado com sucesso!!')->flash();
            redirect('users'.$user->id/"editar");
        }else{
            $this->message->errpr($user->message()->getText())->flash();
            redirect('users'.$user->id/"editar");
        }

    }

    public function excluir(array $request)
    {
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);
        $userDelete = (new User())->findById($request["id"]);

        if (!$userDelete) {
            $this->message->error("Você tentnou deletar um usuário que não existe")->flash();
            redirect('users');
        }

        $userDelete->destroy();

        $this->message->success("O usuário foi excluído com sucesso...")->flash();
        redirect('users');
    }

    public function error(array $data): void
    {
        echo "<h1> Error </h1>";
    }
}