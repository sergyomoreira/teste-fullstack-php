<?php


namespace App\Controllers;


use App\Core\Controller;
use App\Models\Auth;
use App\Models\State;

/**
 * Class State
 * @package App\Controllers
 */
class States extends Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!Auth::user()){
            redirect('login');
        }
    }

    public function index(): void
    {
        $states = (new State())->find()->fetch(true);

        echo $this->load('states/index',[
            "url_base" => url(),
            "user"=> Auth::user(),
            "states" => $states,
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }


    public function create(): void
    {

        echo $this->load('states/create',[
            "url_base" => url(),
            "user"=> Auth::user(),
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }

    public function store(array $request)
    {
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $addState = (new State())->bootstrap(
            $request['name'],
            $request['uf']
        );

        if (($addState->save())){
            $this->message->success('Estado cadastrado com sucesso!! :D')->flash();
            redirect('states');
        }else{
            $this->message->success($addState->message()->getText())->flash();
            redirect('states');
        }
    }

    public function edit($request)
    {

        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $idState = intval($request['id']);

        $state = (new State())->findById($idState);

        if(!$state){
            $this->message->error('Estado não encontrado!! :/')->flash();
            redirect('states');
        }

        echo $this->load('states/edit',[
            "url_base" => url(),
            "user"=> Auth::user(),
            "state" => $state,
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }

    public function update(array $request)
    {

        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $state = (new State())->findById(intVal($request['id']));

        if(!$state){
            $this->message->error('Estado não encontrado!! :/')->flash();
            redirect('states');
        }

        $state->name = $request['name'];
        $state->uf = $request['uf'];

        if (($state->save())){
            $this->message->success('Estado atualizado com sucesso!! :D')->flash();
            redirect('states');
        }else{
            $this->message->error($state->message()->getText())->flash();
            redirect('states');
        }
    }

    public function excluir(array $request)
    {
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $stateDelete = (new State())->findById($request["id"]);

        if (!$stateDelete) {
            $this->message->error("Você tentou excluir um estado que não existe!! :/")->flash();
            redirect('states');
        }

        $stateDelete->destroy();

        $this->message->success("O estado foi excluído com sucesso!! :D")->flash();
        redirect('states');
    }

    public function error(array $data): void
    {
        echo "<h1> Error </h1>";
    }
}