<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Models\Auth;

/**
 * Class Login
 * @package App\Core\Controller
 */
class Login extends Controller
{
    /**
     * Login constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Admin access redirect
     */
    public function root(): void
    {
        $user = Auth::user();

        if ($user) {
            redirect("/admin");
        } else {
            redirect("/login");
        }
    }

    /**
     * @param array|null $data
     */
    public function login(): void
    {
        echo $this->load('login/index', [
            "url_base" => url(),
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }

    public function dologin(?array $request)
    {
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $user = Auth::user();

        if ($user) {
            redirect("/admin/");
        }

        if (!empty($request["email"]) && !empty($request["password"])) {

            $auth = new Auth();
            $login = $auth->login($request["email"], $request["password"], true, 5);

            if ($login) {
                redirect("/admin/");
            } else {
                $this->message->error($auth->message()->getText())->flash();
                redirect('login');
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        redirect('login');
    }

    public function error(array $request): void
    {

        var_dump($request);
    }
}