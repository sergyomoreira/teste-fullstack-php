<?php


namespace App\Controllers;


use App\Core\Controller;
use App\Models\Auth;
use App\Models\Slider;
use App\Support\Upload;

class Sliders extends Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!Auth::user()){
            redirect('login');
        }
    }

    public function index(): void
    {
        $sliders = (new Slider())->find()->fetch(true);

        echo $this->load('sliders/index', [
            "url_base" => url(),
            "user"=> Auth::user(),
            "sliders" => $sliders,
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }

    public function create()
    {

        echo $this->load('sliders/create', [
            "url_base" => url(),
            "user"=> Auth::user(),
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }

    public function store(array $request)
    {
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        if (!empty($_FILES["photo"])) {
            $files = $_FILES["photo"];
            $upload = new Upload();
            $image = $upload->image($files, uniqid(substr(hash('md5', time()), 0, 10)), 800);

            if ($image) {

                $addSlider = (new Slider())->bootstrap(
                    $request['title'],
                    $image
                );

                if (($addSlider->save())) {
                    $this->message->success('Slider atualizado com sucesso!!')->flash();
                    redirect('sliders');
                } else {
                    $this->message->error($addSlider->message()->getText())->flash();
                    redirect('sliders/cadastrar');
                }
            } else {
                $this->message->error('Ops! Erro ao carregar a imagem!!')->flash();
                redirect('sliders/cadastrar');
            }
        }else{
            $this->message->error('Ops! Você precisa fazer uploade de uma foto!!')->flash();
            redirect('sliders/cadastrar');
        }
    }

    public function edit(array $request)
    {

        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $idSlider = intval($request['id']);
        $slider = (new Slider())->findById($idSlider);

        echo $this->load('sliders/edit', [
            "url_base" => url(),
            "user"=> Auth::user(),
            "slider" => $slider,
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }

    public function update(array $request)
    {
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        if (empty($request['id'])) {
            $this->message->error('Ops! Não encontrei o ID para atualizar!! :/')->flash();
            redirect('sliders/');
        }

        $slider = (new Slider())->findById($request['id']);

        if (!empty($_FILES["photo"])) {
            $files = $_FILES["photo"];
            $upload = new Upload();
            $image = $upload->image($files, uniqid(substr(hash('md5', time()), 0, 10)), 800);

            if ($image) {
                $upload->remove($slider->cover);
                $slider->cover = $image;

            } else {
                $this->message->error('Ops! Não consegui carregar a imagem!! :/')->flash();
                redirect('editar/'.$slider->id.'editar');
            }
        }

        $slider->title = $request['title'];

        if (($slider->save())) {
            $this->message->success('Opaa!! Slider adicionado com sucesso!! ;D')->flash();
            redirect('editar/'.$slider->id.'editar');
        } else {
            $this->message->error($slider->message()->getText())->flash();
            redirect('editar/'.$slider->id.'editar');
        }
    }

    public function excluir(array $request)
    {
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $sliderDelete = (new Slider())->findById($request["id"]);

        if (!$sliderDelete) {
            $this->message->error("Você tentou excluir um slider que não existe!! :/")->flash();
            redirect('slider');
        }

        $sliderDelete->destroy();

        $this->message->success("O slider foi excluído com sucesso!! ;D")->flash();
        redirect('sliders');
    }
    public function error(array $data): void
    {
        echo "<h1> Error </h1>";
    }
}