<?php


namespace App\Controllers;


use App\Core\Controller;
use App\Core\Session;
use App\Models\Auth;
use App\Models\Post;
use App\Models\State;
use App\Support\Message;
use App\Support\Upload;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Posts extends Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!Auth::user()){
            redirect('login');
        }
    }

    public function index(): void
    {
        $posts = (new Post())->find()->fetch(true);
        echo $this->load('posts/index', [
            "url_base" => url(),
            "user"=> Auth::user(),
            "posts" => $posts,
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);

    }

    public function create()
    {
        $states = (new State())->find()->fetch(true);
        echo $this->load('posts/create', [
            "url_base" => url(),
            "user"=> Auth::user(),
            "states" => $states,
            "message" => ($this->session->has("flash")) ? $this->flash() : null
        ]);
    }

    public function store(array $request)
    {
        $content = $request["content"];
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $postCreate = new Post();

        $postCreate->author = $request["author"];
        $postCreate->state = $request["state"];
        $postCreate->title = $request["title"];
        $postCreate->uri = str_slug($postCreate->title);
        $postCreate->subtitle = $request["subtitle"];
        $postCreate->content = str_replace(["{title}"], [$postCreate->title], $content);
        $postCreate->video = $request["video"];

        //upload cover
        if (!empty($_FILES["cover"])) {
            $files = $_FILES["cover"];
            $upload = new Upload();
            $image = $upload->image($files, $postCreate->title);

            if (!$image) {
                $this->message->error($upload->message()->getText())->flash();
                redirect('posts');
            }
            $postCreate->cover = $image;
        }

        if (!$postCreate->save()) {

            $this->message->error($postCreate->message()->getText())->flash();
            redirect('posts');

        }
        $this->message->success("Artigo publicado com sucesso...")->flash();
        redirect(url("/posts/" . $postCreate->id . "/editar"));
    }

    public function edit($request)
    {

        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $states = (new State())->find()->fetch(true);
        $post = (new Post())->findById($request['id']);

        if(!$post){
            $this->message->error("Não foi encontrado nenhum artigo!! :/")->flash();
            redirect('posts');
        }

        echo $this->load('posts/edit', [
            "url_base" => url(),
            "user"=> Auth::user(),
            "states" => $states,
            "post" => $post,
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);

    }

    public function update(array $request)
    {

        $content = $request["content"];
        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $post = (new Post())->findById($request['id']);

        if (!$post) {
            $this->message->error("Não foi encontrado nenhum artigo!! :/")->flash();
            redirect('posts');
        }

        $post->author = $request["author"];
        $post->state = $request["state"];
        $post->title = $request["title"];
        $post->uri = str_slug($post->title);
        $post->subtitle = $request["subtitle"];
        $post->content = str_replace(["{title}"], [$post->title], $content);
        $post->video = $request["video"];

        //upload new cover and delete older
        if (!empty($_FILES["cover"])) {
            $files = $_FILES["cover"];
            $upload = new Upload();
            $image = $upload->image($files, $post->title);

            if (!$image) {
                $this->message->error($upload->message()->getText())->flash();
                redirect('posts/'.$post->id."editar");
            }

            $upload->remove($post->cover);
            $post->cover = $image;
        }

        if (!$post->save()) {

            $this->message->error($post->message()->getText())->flash();
            redirect('posts');

        }
        $this->message->success("Artigo atualizado com sucesso...")->flash();
        redirect(url("/posts/" . $post->id . "/editar"));
    }

    public function searchPost($request)
    {

        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);
        $response['posts'] = (new Post())->findByState($request['id']);

        echo json_encode($response);
    }

    public function error(array $data): void
    {
        var_dump($data);
    }
}