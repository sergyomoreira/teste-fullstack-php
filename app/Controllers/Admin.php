<?php


namespace App\Controllers;


use App\Core\Controller;
use App\Models\Auth;

class Admin extends Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!Auth::user()){
            redirect('login');
        }
    }

    public function index(): void
    {
        echo $this->load('admin/index',[
            "url_base" => url(),
            "user"=> Auth::user(),
            "message" => ($this->session->has("flash")) ? $this->session->flash() : null
        ]);
    }

    public function logoff(): void
    {
        $this->message->success("Você saiu com sucesso {$this->user->first_name}.")->flash();

        Auth::logout();
        redirect("/admin/login");
    }

    public function error(array $data): void
    {
        echo "<h1> Error </h1>";
    }
}