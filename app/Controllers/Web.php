<?php


namespace App\Controllers;


use App\Core\Controller;
use App\Models\Post;
use App\Models\Slider;
use App\Models\State;

/**
 * Class Web
 * @package App\Controllers
 */
class Web extends Controller
{
    public function index(): void
    {
        echo $this->load('web/index',[
            "url_base" => url(),
            "sliders" => (new Slider())->find()->fetch(true),
            "states" => (new State())->find()->fetch(true)
            ]);
    }

    public function post(array $request): void
    {

        $request = filter_var_array($request, FILTER_SANITIZE_STRIPPED);

        $post = (new Post())->findById($request['id']);
        echo $this->load('web/post',[
            "url_base" => url(),
            "post" => $post
        ]);
    }

    /**
     * @param array $data
     */
    public function error(array $data): void
    {
        var_dump($data);
    }
}