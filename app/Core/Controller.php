<?php

namespace App\Core;


use App\Models\Auth;
use App\Support\Message;
use \Twig\Loader\FilesystemLoader;
use \Twig\Environment;

/**
 * Class Controller
 * @package App\Core
 */
class Controller
{
    /** @var View */
    protected $view;
    protected $message;
    protected $session;
    /**
     * Controller constructor.
     * @param string|null $view
     */
    public function __construct()
    {
        $this->message = new Message();
        $this->session = new Session();
    }

    protected function load(string $view = null, $params = [] )
    {
        chdir('resources/views/');
        $this->view = new Environment(
            new FilesystemLoader(getcwd()),$params);

        echo $this->view->render($view.'.twig.php', $params);
    }
}