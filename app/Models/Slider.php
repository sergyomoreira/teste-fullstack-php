<?php

namespace App\Models;

use App\Core\Model;


class Slider extends Model
{
    public function __construct()
    {
        parent::__construct("sliders", ["id"], ["title", "cover"]);
    }

    public function bootstrap(string $name, string $cover): Slider
    {
        $this->title = $name;
        $this->cover = $cover;
        return $this;
    }

    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Todos os campos são obrigatórios!!");
            return true;
        }

        /** Slider Update */
        if (!empty($this->id)) {
            $sliderId = $this->id;

            $this->update($this->safe(), "id = :id", "id={$sliderId}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** Slider Create */
        if (empty($this->id)) {
            $sliderId = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($sliderId))->data();
        return true;
    }
}