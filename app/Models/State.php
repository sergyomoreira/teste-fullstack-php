<?php

namespace App\Models;

use App\Core\Model;


/**
 * Class State
 * @package App\Models
 */
class State extends Model
{
    /**
     * State constructor.
     */
    public function __construct()
    {
        parent::__construct("states", ["id"], ["name", "uf"]);
    }

    /**
     * @param string $name
     * @param string $uf
     * @return State
     */
    public function bootstrap(string $name, string $uf): State
    {
        $this->name = $name;
        $this->uf = $uf;
        return $this;
    }

    /**
     * @param string $name
     * @param string $columns
     * @return null|State
     */
    public function findByName(string $name, string $columns = "*"): ?State
    {
        $find = $this->find("name = :name", "name={$name}", $columns);
        return $find->fetch();
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Todos os campos são obrigatórios!!");
            return true;
        }

        /** State Update */
        if (!empty($this->id)) {
            $stateId = $this->id;

            if ($this->find("name = :e AND id != :i", "e={$this->name}&i={$stateId}", "id")->fetch()) {
                $this->message->warning("O estado informado já está cadastrado");
                return false;
            }

            $this->update($this->safe(), "id = :id", "id={$stateId}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** State Create */
        if (empty($this->id)) {
            if ($this->findByName($this->name, "id")) {
                $this->message->warning("O estado informado já está cadastrado");
                return false;
            }

            $stateId = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($stateId))->data();
        return true;
    }
}