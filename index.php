<?php

use App\Core\Session;
use CoffeeCode\Router\Router;

ob_start();

require __DIR__."/vendor/autoload.php";

$session = new Session();
$route = new Router(url(),"@");

//WEB ROUTES

$route->namespace("App\Controllers");
$route->get("/","Web@index");
$route->get("/{id}/post","Web@post");


//ADMIN ROUTES

$route->namespace("App\Controllers")->group("admin");
$route->get("/","Admin@index");

//USERS ROUTES
$route->namespace("App\Controllers")->group("users");
$route->get("/","Users@index");
$route->get("/cadastrar","Users@create");
$route->post("/store","Users@store");
$route->get("/{id}/editar","Users@edit");
$route->post("/update","Users@update");
$route->get("/{id}/excluir","Users@excluir");

//SLIDERS ROUTES
$route->namespace("App\Controllers")->group("sliders");
$route->get("/","Sliders@index");
$route->get("/cadastrar","Sliders@create");
$route->post("/store","Sliders@store");
$route->get("/{id}/editar","Sliders@edit");
$route->post("/update","Sliders@update");
$route->get("/{id}/excluir","Sliders@excluir");

//STATES ROUTES
$route->namespace("App\Controllers")->group("states");
$route->get("/","States@index");
$route->get("/cadastrar","States@create");
$route->post("/store","States@store");
$route->get("/{id}/editar","States@edit");
$route->post("/update","States@update");
$route->get("/{id}/excluir","States@excluir");

//POSTS ROUTES
$route->namespace("App\Controllers")->group("posts");
$route->get("/","Posts@index");
$route->get("/cadastrar","Posts@create");
$route->get("/teste","Posts@teste");
$route->post("/store","Posts@store");
$route->get("/{id}/editar","Posts@edit");
$route->post("/update","Posts@update");

//LOGIN ROUTES
$route->namespace("App\Controllers")->group("login");
$route->get("/","Login@login");
$route->post("/dologin", "Login@dologin");
$route->get("/logout", "Login@logout");

//ERROR ROUTES

$route->namespace("App\Controllers")->group("ops");

$route->get("/{errcode}","Web@error");

//ROUTER DISPATCH

$route->dispatch();

//ERROR REDIRECT

if ($route->error()){
    $route->redirect("/ops/{$route->error()}");
}



ob_end_flush();