{% extends "layout.twig.php" %}

{% block breadcrumb %}
<div class="container w-50 py-5">
    <div class="row">
        <h3>Cadastro de estado</h3>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Início</a></li>
            <li class="breadcrumb-item active" aria-current="page">Novo estado</li>
        </ol>
    </nav>
{% endblock %}

{% block content %}
    <form method="post" action="{{ url_base }}/states/store" enctype="multipart/form-data">
        <div class="row justify-content-center">
            <div class="col-6">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" name="name" class="form-control" placeholder="Nome do estado" autofocus>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="uf">UF</label>
                    <input type="text" name="uf" class="form-control" placeholder="Sigla do estado" autofocus>
                </div>
            </div>
        </div>
        <div class="row justify-content-center ">
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Enviar</button>
                <a href="{{ url_base }}/users" class="btn btn-danger">Cancelar</a>
            </div>
        </div>
    </form>
</div>
{% endblock %}

{% block js %}

{% endblock %}