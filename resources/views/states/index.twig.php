{% extends "layout.twig.php" %}

{% block breadcrumb %}
<div class="container w-50 py-5">
    <div class="row">
        <h3>Listagem de estados</h3>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Início</a></li>
            <li class="breadcrumb-item active" aria-current="page">Estados</li>
        </ol>
    </nav>
{% endblock %}

{% block content %}
<div class="row py-3">
    <div class="col-12 text-left">
        <a href="{{ url_base }}/states/cadastrar" class="btn btn-primary">Adicionar novo</a>
    </div>

</div>
<table class="table table-striped text-center">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Nome</th>
        <th scope="col">UF</th>
        <th scope="col">Ações</th>
    </tr>
    </thead>
    <tbody>
    {% for state in states %}
    <tr>
        <th scope="row">{{ state.id }}</th>
        <td>{{ state.name }}</td>
        <td>{{ state.uf }}</td>
        <td>
            <a href="{{ url_base }}/states/{{ state.id }}/editar" class="py-2">
                <i class="fas fa-edit text-dark"></i>
            </a>
            <a href="{{ url_base }}/states/{{ state.id }}/excluir" class="py-2">
                <i class="fas fa-trash-alt text-dark"></i>
            </a>
        </td>
    </tr>
    {% endfor %}
    </tbody>
</table>
</div>
{% endblock %}