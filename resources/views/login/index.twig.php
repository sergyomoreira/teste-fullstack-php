{% extends "layout.twig.php" %}
{% block content %}
<div class="container w-50 py-5">
    <div class="row justify-content-center">
        <form class="form-signin text-center w-50" method="post" action="{{ url_base }}/login/dologin">
            <h1 class="h3 mb-3 font-weight-normal">Faça login</h1>
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" name="email" class="form-control" placeholder="Seu email">
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="form-group">
                        <label for="password">Senha</label>
                        <input type="password" name="password" class="form-control" placeholder="Senha">
                    </div>
                </div>
            </div>
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" value="remember-me"> Lembrar de mim
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2021-2022</p>
        </form>
    </div>
</div>
{% endblock %}