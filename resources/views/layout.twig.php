<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css">
    <link rel="stylesheet" href="{{ url_base }}/public/css/app.css">
    <link rel="stylesheet" href="{{ url_base }}/public/css/jquery.toast.css">
    {% block css %}

    {% endblock %}
    <title>{% block title %}{% endblock %} - Phidelis Tecnologia</title>


</head>
<body class="bg-light">
<header>
    <div class="container">
        <nav class="navbar navbar-light bg-light py-3">
            <a class="navbar-brand" href="#">
                <img src="{{ url_base }}/public/img/logo.png" width="90" height="30" class="d-inline-block align-top"
                     alt="">
            </a>
            <span class="navbar-text">
                <a href="{{ url_base }}/" class="mr-3"> Home</a>
                {% if (user)  %}
                    <a href="{{ url_base }}/login/logout" class="btn btn-primary px-3 text-white"
                       style="border-radius: 100px;">Sair</a>
                {% else %}
                    <a href="{{ url_base }}/login/" class="btn btn-primary px-3 text-white"
                       style="border-radius: 100px;">Login</a>
                {% endif %}
            </span>
        </nav>
    </div>
</header>

<main>
    <section>
        {% block breadcrumb %}{% endblock %}

        {% if message %}
            <div class="bg-{{message.type}} text-white p-3 mb-2 text-center">{{message.text}}</div>
        {% endif %}

        {% block content %}{% endblock %}
    </section>
</main>

<footer class="mt-auto fixed-bottom">
    <div class="text-center align-items-center py-2 position-relative" style="background-color: #366aba;">
        <p class="p-0 text-white">
            <a href="https://www.phidelis.com.br/">
                <strong class="text-white ">Phidelis Tecnologia</strong>
            </a>
        </p>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
<script src="{{ url_base }}/public/js/tinymce/jquery.tinymce.min.js"></script>
<script src="{{ url_base }}/public/js/tinymce/tinymce.min.js"></script>
<script src="{{ url_base }}/public/js/scripts.js"></script>


{% block js %}

{% endblock %}
</body>
</html>