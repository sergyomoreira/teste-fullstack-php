{% extends "layout.twig.php" %}

{% block breadcrumb %}
<div class="container w-50 py-5">
    <div class="row">
        <h3>Cadastro de artigo</h3>
    </div>

    <nav aria-label="breadcrumb" class="py-4">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Início</a></li>
            <li class="breadcrumb-item active" aria-current="page">Artigos</li>
        </ol>
    </nav>
    {% endblock %}

    {% block content %}

    <div class="row">
        <a href="{{ url_base }}/posts/cadastrar" class="btn btn-success m-3" style="border-radius: 100px;">Adicionar novo</a>
    </div>

    <table class="table table-striped text-center">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Título</th>
                <th scope="col">Autor</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
            {% for post in posts %}
                <tr>
                    <th scope="row">{{ post.id }}</th>
                    <td>{{ post.title }}</td>
                    <td>{{ post.author().first_name }} {{ post.author().last_name }}</td>
                    <td>
                        <a href="{{ url_base }}/posts/{{ post.id }}/editar" class="py-2">
                            <i class="fas fa-edit text-dark"></i>
                        </a>
                        <a href="{{ url_base }}/posts/{{ post.id }}/excluir" class="py-2">
                            <i class="fas fa-trash-alt text-dark"></i>
                        </a>
                    </td>
                </tr>
            {% endfor %}
        </tbody>
    </table>
</div>
{% endblock %}

{% block js %}


{% endblock %}