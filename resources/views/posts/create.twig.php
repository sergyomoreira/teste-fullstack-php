{% extends "layout.twig.php" %}

{% block breadcrumb %}
<div class="container w-50 py-5">
    <div class="row">
        <h3>Cadastro de artigo</h3>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Início</a></li>
            <li class="breadcrumb-item active" aria-current="page">Novo artigo</li>
        </ol>
    </nav>
    {% endblock %}

    {% block content %}
    <form enctype="multipart/form-data" method="post" action="{{ url_base }}/posts/store">
        <input type="hidden" name="author" value="1">
        <div class="row">
            <div class="col-4">
                <label for="state">Estado:</label>
                <select name="state" required class="form-control">
                    {% for state in states %}
                    <option value="{{ state.id }}">{{ state.name }}</option>
                    {% endfor %}
                </select>
            </div>
            <div class="col-4">
                <label for="state">Vídeo:</label>
                <input type="text" name="video" class="form-control" placeholder="O ID de um vídeo do YouTube"/>
            </div>
            <div class="col-4">
                <label for="state">Imagem da capa:</label>
                <input type="file" name="cover" placeholder="Uma imagem de capa"/>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="title">Título</label>
                    <input type="text" name="title" class="form-control" placeholder="A manchete do seu artigo">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="subtitle">Subtítulo</label>
                    <input type="text" name="subtitle" class="form-control" placeholder="A manchete do seu artigo">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="subtitle">Conteúdo</label>
                    <textarea class="form-control mce" name="content"></textarea></div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
        <a href="{{ url_base }}/posts" class="btn btn-danger">Cancelar</a>
    </form>
</div>
{% endblock %}