{% extends "layout.twig.php" %}

{% block content %}
<div class="container w-50 py-5">
    <div class="row">
        <h3>Dashboard</h3>
    </div>
    <div class="row py-5 text-center">
        <div class="col-6">
            <a href="{{ url_base }}/posts" class="btn btn-primary py-3 px-5">Publicações</a>
        </div>
        <div class="col-6">
            <a href="{{ url_base }}/states" class="btn btn-primary py-3 px-5">Estados</a>
        </div>
    </div>
    <div class="row py-5 text-center">
        <div class="col-6">
            <a href="{{ url_base }}/sliders" class="btn btn-primary py-3 px-5">Slider</a>
        </div>
        <div class="col-6">
            <a href="{{ url_base }}/users" class="btn btn-primary py-3 px-5">Usuários</a>
        </div>
    </div>
</div>

{% endblock %}