{% extends "layout.twig.php" %}

{% block breadcrumb %}
<div class="container w-50 py-5">
    <div class="row">
        <h3>Listagem de usuários</h3>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Início</a></li>
            <li class="breadcrumb-item active" aria-current="page">Usuários</li>
        </ol>
    </nav>
{% endblock %}

{% block content %}
<div class="row py-3">
    <div class="col-12 text-left">
        <a href="{{ url_base }}/users/cadastrar" class="btn btn-primary">Adicionar novo</a>
    </div>

</div>
<table class="table table-striped text-center">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Usuário</th>
        <th scope="col">E-mail</th>
        <th scope="col">Ações</th>
    </tr>
    </thead>
    <tbody>
    {% for user in users %}
    <tr>
        <th scope="row">{{ user.id }}</th>
        <td>{{ user.first_name }} {{ user.last_name }}</td>
        <td>{{ user.email }}</td>
        <td>
            <a href="{{ url_base }}/users/{{ user.id }}/editar" class="py-2">
                <i class="fas fa-edit text-dark"></i>
            </a>
            <a href="{{ url_base }}/users/{{ user.id }}/excluir" class="py-2">
                <i class="fas fa-trash-alt text-dark"></i>
            </a>
        </td>
    </tr>
    {% endfor %}
    </tbody>
</table>
</div>

{% endblock %}