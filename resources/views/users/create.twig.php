{% extends "layout.twig.php" %}

{% block breadcrumb %}
<div class="container w-50 py-5">
<div class="row">
    <h3>Cadastro de usuários</h3>
</div>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page">Novo usuário</li>
    </ol>
</nav>
{% endblock %}

{% block content %}
<form method="post" action="{{ url_base }}/users/store">

<div class="row justify-content-center">
    <div class="col-6">
        <div class="form-group">
            <label for="first_name">Nome</label>
            <input type="text" name="first_name" class="form-control" placeholder="Seu nome" autofocus>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="last_name">Sobrenome</label>
            <input type="text" name="last_name" class="form-control" placeholder="Seu sobrenome">
        </div>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-12">
        <div class="form-group">
            <label for="email">Endereço de email</label>
            <input type="email" name="email" class="form-control" placeholder="Seu email">
        </div>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-12">
        <div class="form-group">
            <label for="password">Senha</label>
            <input type="password" name="password" class="form-control" placeholder="Senha">
        </div>
    </div>
</div>
<div class="row justify-content-center ">
    <div class="col-12 text-center">
        <button type="submit" class="btn btn-primary">Enviar</button>
        <a href="{{ url_base }}/users" class="btn btn-danger">Cancelar</a>
    </div>
</div>
</form>
</div>>
{% endblock %}

{% block js %}

{% endblock %}