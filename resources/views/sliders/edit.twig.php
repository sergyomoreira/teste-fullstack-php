{% extends "layout.twig.php" %}

{% block breadcrumb %}
<div class="container w-50 py-5">
    <div class="row">
        <h3>Editando slider com id: {{ slider.id }}</h3>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Início</a></li>
            <li class="breadcrumb-item active" aria-current="page">Novo slider</li>
        </ol>
    </nav>
    {% endblock %}

    {% block content %}
    <form method="post" action="{{ url_base }}/sliders/update" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{slider.id}}">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="form-group">
                    <label for="title">Nome</label>
                    <input type="text" name="title" class="form-control"  value="{{slider.title}}" placeholder="Titulo do slider" autofocus>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="form-group">
                    <label for="photo">Foto</label>
                    <input type="file" name="photo" class="form-control"/>
                </div>
            </div>
        </div>
        <div class="row justify-content-center ">
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Enviar</button>
                <a href="{{ url_base }}/sliders" class="btn btn-danger">Cancelar</a>
            </div>
        </div>
    </form>
</div>
{% endblock %}

{% block js %}

{% endblock %}