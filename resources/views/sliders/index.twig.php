{% extends "layout.twig.php" %}

{% block css %}
<style>
    section div:first-child {
        background-color: red;
        width: 150px;
        height: 150px;
        position: relative;
    }
    section div:last-child {
        background-color: blue;
        width: 150px;
        height: 150px;
        position: relative;
        margin-top: -130px;
        margin-left: 20px;
    }
</style>

{% endblock %}

{% block breadcrumb %}
<div class="container w-50 py-5">
    <div class="row">
        <h3>Listagem de sliders</h3>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Início</a></li>
            <li class="breadcrumb-item active" aria-current="page">Sliders</li>
        </ol>
    </nav>
{% endblock %}

{% block content %}
<div class="row py-3">
    <div class="col-12 text-left">
        <a href="{{ url_base }}/sliders/cadastrar" class="btn btn-primary">Adicionar novo</a>
    </div>

</div>
<table class="table table-striped text-center">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Nome</th>
        <th scope="col">Ações</th>
    </tr>
    </thead>
    <tbody>
    {% for slider in sliders %}
    <tr>
        <th scope="row">{{ slider.id }}</th>
        <td>{{ slider.title }}</td>
        <td>
            <a href="{{ url_base }}/sliders/{{ slider.id }}/editar" class="py-2">
                <i class="fas fa-edit text-dark"></i>
            </a>
            <a href="{{ url_base }}/sliders/{{ slider.id }}/excluir" class="py-2">
                <i class="fas fa-trash-alt text-dark"></i>
            </a>
        </td>
    </tr>
    {% endfor %}
    </tbody>
</table>
</div>

{% endblock %}