{% extends "layout.twig.php" %}

{% block css %}
<style>
    .banner{
        background-color: #DEF1FE;
        width: 100%;
        height: 300px;
        position: relative;
    }

    .video{
        position: relative;
        margin-top: -300px;
        margin-left: 65%;
    }
    .titulo{
        width: 40%;
        padding-left: 20%;
        padding-top: 5%;
    }
    .author{
        padding-left: 20%;
    }
    .conteudo{
        width: 100vw;
        height: 100vh;
        display: flex;
        flex-direction: row;
        justify-content: center;
        padding-top: 5%;
    }
    .box {
        width: 50%;
        height: 300px;
        background: #fff;
    }
</style>

{% endblock %}

{% block content %}
<section>
    <div class="banner">
        <div class="titulo">
            <h1>
                {{post.title}}
            </h1>
        </div>
    </div>
    <div class="author">
        <p class="py-4">{{ post.author().first_name }} {{ post.author().last_name }}</p>
    </div>
    <div class="video">
        <iframe width="450" height="315" src="https://www.youtube.com/embed/{{post.video}}"></iframe>
    </div>
</section>
<div class="conteudo">
    <div class="box">
        {{post.content}}
    </div>
</div>

{% endblock %}