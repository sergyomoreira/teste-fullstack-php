{% extends "layout.twig.php" %}

{% block content %}
<div class="container">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active mx-auto">
                <img class="d-block w-100" width="800" height="400" src="public/img/img-hero.png" alt="Primeiro Slide">
                <div class="carousel-caption d-none d-md-block py-3 text-dark text-left">
                    <h1 class="p-2 w-50">Descubra diversos lugares e as melhores experiências aqui!</h1>
                    <p class="p-5">Assista o vídeo</p>
                </div>
            </div>
            {% for slider in sliders %}
            <div class="carousel-item mx-auto">
                <img class="d-block w-100" width="800" height="400" src="{{slider.cover}}" alt="Primeiro Slide">
                <div class="carousel-caption d-none d-md-block py-3 text-dark text-left">
                    <h1 class="p-2 w-50">{{ slider.title }}</h1>
                </div>
            </div>
            {% endfor %}
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Próximo</span>
        </a>
    </div>
</div>
<div class="container">
    <section class="jumbotron text-center">
        <div class="container">
            <h2 class="jumbotron-heading">Viaje conosco pelo Brasil</h2>
            <p class="lead text-muted">
                O objetivo do blog é compartilhar as experiências vivenciadas nas viagens pelo Brasil.
            </p>
        </div>
    </section>
</div>
<div class="container">
    <section>
        <form method="post" action="{{ url_base }}/users/store">
            <div class="form-group">
                <h3>Comece sua busca</h3>
                <input type="text" name="first_name" class="form-control" placeholder="Seu nome" autofocus>
            </div>
        </form>
    </section>
</div>


<section>
    <div class="container py-5">
        <div class="row my-4">
            <div class="form-group">
                <h3>Selecione o estado:</h3>
                {% for state in states %}
                    <a href="#" class="btn btn-primary px-3 text-white" style="border-radius: 100px;">{{state.name}}</a>
                {% endfor %}
            </div>
        </div>
        <div class="row my-3">
            <div class="form-group">
                <h3>Principais viagens</h3>

            </div>
        </div>
    </div>

</section>

{% endblock %}
{% block js %}
<script>
    postSearch(1)
</script>

{% endblock %}